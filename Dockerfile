FROM node:12.18.3-slim

WORKDIR /usr/src/app

COPY package.json yarn.lock ./

COPY src ./src

RUN yarn install --quiet

CMD [ "yarn", "start" ]